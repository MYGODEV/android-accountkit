Account Kit SDK for Android Samples
========================

This repository contains a collection of samples demonstrating how to integrate the Account Kit SDK.

Learn more about the provided samples, documentation, integrating the SDK into your app, accessing source code, and more at https://developers.facebook.com/docs/accountkit

Reference
========================
The source code is from github.com (https://github.com/fbsamples/account-kit-samples-for-android/).